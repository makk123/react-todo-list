import TodoReducer from "./TodoReducer";
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    TodoReducer

})

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;