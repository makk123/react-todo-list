import React from 'react';
import { ITask } from './interfaces';
import { FC, ChangeEvent, useState } from 'react';

const Edit = () => {
	const [task, setTask] = useState<string>('');
	const [todoList, setTodoList] = useState<ITask[]>([]);
	const [editingText, setEditingText] = useState('');
	const [todoEditing, setTodoEditing] = useState<number>();



	const addTask = (): void => {
		const newTask = { taskId: Date.now(), taskName: task };
		setTodoList([...todoList, newTask]);
		setTask('');
		setEditingText('');
		setTodoEditing(undefined);
	};

	const editTask = (taskIdToEdit: number): void => {
		const task = todoList.find((todo) => todo.taskId === taskIdToEdit);
		if (task) {
			setTodoEditing(taskIdToEdit);
			setEditingText(task.taskName);
			
		}
	};

	const updateTask = (): void => {
		const updatedTodos = [...todoList].map((task) => {
			if (task.taskId === todoEditing) {
				task.taskName = editingText;
				console.log(editingText);
			}
			return task;
		});
		setTodoList(updatedTodos);
		setEditingText('');
		setTodoEditing(undefined);
	};
	return (
		<div>
			{/* <p>{localStorage.getItem('Task')}</p>
			<p>{localStorage.getItem('edit')}</p> */}
			<h1>Edit page here!</h1>
                
					<input type="text" onChange={(event) => setEditingText(event.target.value)} value={editingText} />
					<div className="edit button">
						<button onClick={() => updateTask()}>Submit Edits</button>
					</div>
				
       
			

		</div>
	);
};

export default Edit;