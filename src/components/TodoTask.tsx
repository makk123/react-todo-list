/* eslint-disable react/prop-types */
import React from 'react';
import { ITask } from './interfaces';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { deleteTodo, editTodo } from '../action/TodoAction';

interface Props {
	list: ITask;
	deleteTodo:(id: number) => void
	editTodo:(id: number) => void
	addTodo:(id: string) => void
	

}

const TodoTask = ({ list,deleteTodo,editTodo,addTodo }: Props) => {
	
	const dispatch = useDispatch();

	return (
		<div className="content">
              
			<h3>{list.taskName}</h3>
			<button onClick={() => dispatch(deleteTodo(list.taskId))}>Delete Task</button>

		</div>

	);

};


export default TodoTask;
//export default connect()(TodoTask);

// export default connect(mapStateToProps, mapDispatchToProps)(TodoTask);