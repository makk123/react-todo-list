import React, { FC, ChangeEvent, useState, Props } from 'react';
import ReactDOM from 'react-dom';
import { connect, useDispatch } from "react-redux";
import { addTodo,deleteTodo, editTodo } from './action/TodoAction';
import './App.css';
import { ITask } from './components/interfaces';
import TodoTask from './components/TodoTask';
import Edit from './components/Edit'
import { RootState } from './reducer/index';


const mapStateToProps = (state: RootState) => {
	
	return {
		list: state.TodoReducer,
		
		
	};
};
   
const mapDispatchToProps = {
	
	deleteTodo,
	editTodo,
	addTodo,
	

};


const App = ({ list,
		editTodo,
        addTodo,
		deleteTodo,
	}: 
		{
		list: ITask[];
		editTodo: (id: number) => void;
		addTodo: (id: string) => void;
		deleteTodo: (id: number) => void;
		
}) => {
	const [task, setTask] = useState<string>('');
	const [todoList, setTodoList] = useState<ITask[]>([]);
	const [editingText, setEditingText] = useState('');
	const [todoEditing, setTodoEditing] = useState<number>();

	
	const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {

		setTask(event.target.value);

	};


	const dispatch = useDispatch();

	const addTask = (): void => {
		const newTask: ITask = { taskId: Date.now(), taskName: task };
		setTodoList([...todoList, newTask]);
		setTask('');
		setEditingText('');
		setTodoEditing(undefined);
	};

	const deleteItems = (taskIdToDelete: number): void => {
		setTodoList(
			todoList.filter((task) => {
				console.log(task.taskName);
				return task.taskId !== taskIdToDelete;
			})
		);
	};





	const updateTask = (): void => {
		const updatedTodos = [...todoList].map((task) => {
			if (task.taskId === todoEditing) {
				task.taskName = editingText;
				console.log(editingText);
			}
			return task;
		});
		setTodoList(updatedTodos);
		setEditingText('');
		setTodoEditing(undefined);
	};

	const items =  (list as any).list
     

	return (
		
		<div className="App">
			<div className="header">
				<input type="text" placeholder=" Write Task" value={task} name={task} onChange={handleChange} ></input>
				<button onClick={() => dispatch(addTodo(task))}>Add Task</button>
				
			</div>

			<div className="TodoList">
				{todoList.map((list: ITask, key: number) => {
					return <TodoTask key={key}
					list={list}
					deleteTodo ={deleteTodo}
					editTodo ={editTodo}
					addTodo ={addTodo}
					
												
					/>;
                
				})}

			</div>
		</div>

	);
};

// export default App;
export default connect(mapStateToProps, mapDispatchToProps)(App);


